# GNS-3 IPterm extended
The CI/CD of this repo builds and publish a Docker image based on the GNS3 IPTerm.

## Description

This docker image is based on [gns3/ipterm](https://hub.docker.com/r/gns3/ipterm) docker image.

### Additional packages added history

Version 0.2.3
- isc-dhcp-client 
- dnsutils
- wget

## Build locally to test 

### Building
To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t gns3-ipterm-extended .
```

On ARM MAC (with M1/M2 chip) and with docker installed, you can create Intel images with the command:
```
docker buildx build --platform linux/amd64 -t gns3-ipterm-extended .
```

### Running locally to test it
To run the image locally and to test it:
```
docker run -i -t gns3-ipterm-extended /bin/bash
```

## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository--> Tag" menu to add the latest version number.

## How to use this image

To download and use this image, just type:
```
docker pull registry.forge.hefr.ch/bun/dockers/gns3-ipterm-extended
```

## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/


(c) F. Buntschu 15.04.2024